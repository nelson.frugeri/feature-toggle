package com.aws.appconfig.featuretoggle.controller;

import com.aws.appconfig.featuretoggle.adapter.aws.appconfig.domain.Toggle;
import com.aws.appconfig.featuretoggle.adapter.aws.appconfig.domain.Toggle.Feature;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/featuretoggle")
public class FeatureToggleController {

  @Autowired
  private Toggle toggle;

  @ResponseBody
  @GetMapping
  public ResponseEntity<?> find() {
    var flow = "Follow the legacy flow \n";

    if (toggle.getFeatures().stream()
        .filter(feature -> feature.getName().toUpperCase(Locale.ROOT).equals("MODERN"))
        .anyMatch(Feature::isEnabled)) {
      flow = "Follow the modern flow \n";
    }

    return ResponseEntity.status(HttpStatus.OK).body(flow);
  }
}
