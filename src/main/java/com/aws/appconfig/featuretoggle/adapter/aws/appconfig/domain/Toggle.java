package com.aws.appconfig.featuretoggle.adapter.aws.appconfig.domain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "toggle")
@Getter
@Setter
public class Toggle {

  private String name;
  private boolean enabled;
  private List<Feature> features;

  @Getter
  @Setter
  public static class Feature {

    private String name;
    private boolean enabled;
  }
}
