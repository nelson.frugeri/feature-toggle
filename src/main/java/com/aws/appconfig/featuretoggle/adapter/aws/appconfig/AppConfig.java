package com.aws.appconfig.featuretoggle.adapter.aws.appconfig;

import com.amazonaws.services.appconfig.AmazonAppConfig;
import com.amazonaws.services.appconfig.AmazonAppConfigClient;
import com.amazonaws.services.appconfig.model.GetConfigurationRequest;
import com.amazonaws.services.appconfig.model.GetConfigurationResult;
import com.aws.appconfig.featuretoggle.adapter.aws.appconfig.credentials.AwsCredentials;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

  @Value("${cloud.aws.region.static}")
  private String region;

  @Value("${application.name}")
  private String applicationName;

  @Value("${application.env}")
  private String env;

  @Autowired
  private AwsCredentials awsCredentials;

  public GetConfigurationResult getConfiguration(@NonNull final String configuration) {
    var request = new GetConfigurationRequest();

    request.setApplication(applicationName);
    request.setEnvironment(env);
    request.setConfiguration(configuration);
    request.setClientId("app-config");

    return awsAppConfig().getConfiguration(request);
  }

  private AmazonAppConfig awsAppConfig() {
    return AmazonAppConfigClient.builder()
        .withCredentials(awsCredentials.provider())
        .withRegion(region).build();
  }
}
