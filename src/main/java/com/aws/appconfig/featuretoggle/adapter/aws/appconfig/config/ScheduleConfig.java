package com.aws.appconfig.featuretoggle.adapter.aws.appconfig.config;

import com.aws.appconfig.featuretoggle.adapter.aws.appconfig.AppConfig;
import com.aws.appconfig.featuretoggle.adapter.aws.appconfig.domain.Toggle;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@Slf4j
public class ScheduleConfig {

  @Autowired
  private Toggle toggle;

  @Autowired
  private AppConfig appConfig;

  @SneakyThrows
  @Scheduled(cron = "${aws.appConfig.schedule.cron}")
  public void pollConfiguration() {
    if (toggle.isEnabled()) {
      final var featureToggle = new ObjectMapper().readValue(
          new String(appConfig.getConfiguration(toggle.getName()).getContent().array(),
              StandardCharsets.US_ASCII), Toggle.class);

      toggle.setFeatures(featureToggle.getFeatures());
      toggle.setEnabled(featureToggle.isEnabled());

      log.info("Scheduled with values for toggle --- name: " +
          toggle.getName() + " enabled: " + toggle.isEnabled());

      toggle.getFeatures().forEach(feature -> log.info("Features --- name: " +
          feature.getName() + " enabled: " + feature.isEnabled() + "\n"));
    }
  }
}
